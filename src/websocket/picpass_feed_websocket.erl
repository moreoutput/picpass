-module(picpass_feed_websocket, [Req, SessionId]).
-behaviour(boss_service_handler).
-record(state, {users}).
-export([init/0, 
    handle_incoming/4, 
    handle_join/3,
    handle_broadcast/2,
    handle_close/4, 
    handle_info/2,
    terminate/2]).

init() -> {ok, #state{users=dict:new()}}.

handle_join(ServiceName, WebSocketId, State) ->
  #state{users=Users} = State,
  User = dict:store(WebSocketId, [{session, SessionId}], Users),
  {noreply, #state{users=User}}.

handle_close(Reason, ServiceName, WebSocketId, State) ->
  #state{users=Users} = State,
  {noreply, #state{users=dict:erase(WebSocketId, Users)}}.

handle_broadcast(Message, State) ->
  {noreply, State}.

handle_incoming(ServiceName, WebSocketId, Message, State) ->
  {_, IncomingMsg} = mochijson2:decode(Message),
  Loc = proplists:get_value(<<"loc">>, IncomingMsg, "None"),
  {_, [{_, Long}, {_, Lat}]} = Loc,
  Type = proplists:get_value(<<"type">>, IncomingMsg, 0),
  if Type == <<"pic">> -> process_pic(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State);
    Type == <<"dislike">> -> process_dislike(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State); 
    Type == <<"like">> -> process_like(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State);
    Type == <<"flag">> -> process_flag(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State);
    true -> process_user(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State)
  end.

% User connecting
process_user(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State) ->
  {_,SessionList} = dict:find(WebSocketId, State#state.users),
  [Head|Tail] = SessionList,
  {_, Session} = Head,
  TempState = dict:new(),
  NewState = dict:store(WebSocketId, [{session, Session}, {lng, Long}, {lat, Lat}], State#state.users),
  Pics = find_pics(Long, Lat),
  if Pics == [] -> {noreply, {state, NewState}};
    true -> 
      [H|T] = find_pics(Long, Lat),
      Message = pic_to_message(H),
      send(Message, T, WebSocketId),
      {noreply, {state, NewState}}
  end.

process_pic(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State) ->
  Pic = pic:new(id, 
  proplists:get_value(<<"name">>, IncomingMsg, "Unnamed"),  
  proplists:get_value(<<"imgPath">>, IncomingMsg, 0), 
  1, 
  0, 
  proplists:get_value(<<"description">>, IncomingMsg, "No Description"),
  [<<"">>], 
  {lng, Long, lat, Lat}),
  {_,Pic2} = Pic:save(),
  Message = pic_to_message(Pic2),
  broadcast(Message, WebSocketId, Long, Lat, State),
  {noreply, State}.

process_like(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State) ->
  ID = binary_to_list(proplists:get_value(<<"id">>, IncomingMsg, "None")),
  Pic = boss_db:find(ID),
  {_, _, _, _, Likes, _, _, _, {_,_,_,_}} = Pic,
  UpdatedPic = Pic:set( [{like, Likes + 1}] ),
  case UpdatedPic:save() of
    {ok, Saved} -> 
      broadcast({[{type, liked}, {id, list_to_binary(ID)}]}, WebSocketId, Long, Lat, State),
      {noreply, State}
  end.

process_dislike(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State) ->
  ID = binary_to_list(proplists:get_value(<<"id">>, IncomingMsg, "None")),
  Pic = boss_db:find(ID),
  {_, _, _, _, _, Dislikes, _, _, {_,_,_,_}} = Pic,
  UpdatedPic = Pic:set( [{dislike, Dislikes + 1}] ),
  case UpdatedPic:save() of
    {ok, Saved} -> 
      broadcast({[{type, disliked}, {id, list_to_binary(ID)}]}, WebSocketId, Long, Lat, State),
      {noreply, State}
  end.

process_flag(ServiceName, WebSocketId, IncomingMsg, Long, Lat, Type, State) ->
  ID = binary_to_list(proplists:get_value(<<"id">>, IncomingMsg, "None")),
  NewFlag = proplists:get_value(<<"name">>, IncomingMsg, "None"),
  Pic = boss_db:find(ID),
  {_, _, _, _, _, Dislikes, _, Flags, {_,_,_,_}} = Pic,
  UpdatedPic = Pic:set( [{flags, lists:append(Flags, [NewFlag])}]),
  case UpdatedPic:save() of
    {ok, Saved} -> {noreply, State}
  end.

handle_info(state, State) ->
  #state{users=Users} = State,
  error_logger:info_msg("state:~p~n", [Users]),
  {noreply, State};

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

%% Specific to sending JSON data to the correct users

% to users who are not the user
broadcast(Message, WebSocketId, Long, Lat, State) ->
  #state{users=Users} = State,
  AllUsers = dict:fetch_keys(Users),
  send_to(Message, WebSocketId, Long, Lat, [X || X <- AllUsers, X =/= WebSocketId], State).

% send to specific websocket
send(Message, WebSocketId) ->
  WebSocketId ! {text,  mochijson2:encode(Message)}.

send(Message, [H|T], WebSocketId) ->
  send(Message, WebSocketId),
  NewMessage = pic_to_message(H),
  send(NewMessage, T, WebSocketId);

send(Message, [], WebSocketId) -> 
  send(Message, WebSocketId),
  ok;

send(Message, WebSocketId, D) when D >= 1000 -> ok;

send(Message, WebSocketId, D) when D =< 999 ->
  WebSocketId ! {text,  mochijson2:encode(Message)}.

send_to(Message, WebSocketId, Long, Lat, [], State) -> ok;

send_to(Message, WebSocketId, Long, Lat, [H|T], State) ->
  {_,SessionList} = dict:find(H, State#state.users),
  UserLoc = [{lng, getLongitude(SessionList)}, {lat, getLatitude(SessionList)}],
  UserLat =  getLatitude(SessionList),
  UserLong = getLongitude(SessionList),
  D = calculate_distance(Long, Lat, UserLong, UserLat),
  send(Message, H, D),
  send_to(Message, WebSocketId, Long, Lat, T, State).

% haversine formula
calculate_distance(Lng1, Lat1, Lng2, Lat2) ->
  Deg2rad = fun(Deg) -> math:pi()*Deg/180 end,
  [RLng1, RLat1, RLng2, RLat2] = [Deg2rad(Deg) || Deg <- [Lng1, Lat1, Lng2, Lat2]],
  DLon = RLng2 - RLng1,
  DLat = RLat2 - RLat1,
  A = math:pow(math:sin(DLat/2), 2) + math:cos(RLat1) * math:cos(RLat2) * math:pow(math:sin(DLon/2), 2),
  C = 2 * math:asin(math:sqrt(A)),
  Km = 6372.8 * C,
  Km.

find_pics(Long, Lat) ->
  boss_db:find(pic, [{loc, 'near', [Long, Lat]} ], [
    {limit, 15},
    {order_by, like},
    ascending
  ]).

getLatitude([])-> {error, empty};

getLatitude(SessionList) ->
  [H|T] = [X || {lat, X} <- SessionList, X =/= false],
  H.

getLongitude([])-> {error, empty};

getLongitude(SessionList) ->
  [H|T] = [X || {lng, X} <- SessionList, X =/= false],
  H.

getSessionId([]) -> {error, empty};

getSessionId(SessionList)->
  [H|T] = [X || {session, X} <- SessionList, X =/= false],
  H.

pic_to_message(Pic) ->
  {_, ID, Title, URL, Likes, Dislikes, FileName, _, {_,_,_,_}} = Pic,
  {[
    {type, 'piclisting'}, 
    {id, list_to_binary(ID)},
    {title, Title},
    {imgPath, URL},
    {likes, Likes},
    {dislikes, Dislikes},
    {filename, FileName}
  ]}.