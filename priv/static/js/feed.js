$(document).ready(function() {
	'use strict';
	var wsc = new WebSocket('ws://127.0.0.1:8001/websocket/feed'),
	userConnection = {
		type: 'geoconnection',
		loc: {lng: 38.0297, lat: 84.4947}
	},
	loadingTimer,
	imgData = null, // Base64 of image
	fileSelect = function(evt) { // file to base64
	    var files = evt.target.files,
	    file = files[0],
	    reader;

	    if (files && file) {
	        reader = new FileReader();

	        reader.onload = function(readerEvt) {
	            var binaryString = readerEvt.target.result;
	            imgData = window.btoa(binaryString);
	        };

	        reader.readAsBinaryString(file);
	    }
	},
	addPic = function(picObj) {
		var id =  picObj.id,
		flags = ['Language', 'Adult Content', 'Weird'];
		$('#piclistings > ul').prepend('<li id="' + id + '"><div class="pic"><img src="' + picObj.imgPath + '" /></div>' + 
		'<div class="controls"><ul>' +
			'<li class="pic-likes voting"><span class="pure-button pure-button-primary">Likes: <span class="total">' + picObj.likes + '</span></span></li>' + 
			'<li class="pic-dislikes voting"><span class="pure-button pure-button-primary">Dislikes: <span class="total">' + picObj.dislikes + '</span></span></li>' + 
			(function() {
				var i = 0,
				str = '<li class="select-field select-flag"><select id="select-' + id + '"><option value="">Flag this Image for:</option>';
				
				for (i; i < flags.length; i += 1) {
					str += '<option value="' + flags[i] + '">' + flags[i] + '</option>';
				}

				return str + '</select></li>';
			}())
		 + '</ul></div></li>');

		$('#' + id).find('.voting').on('click', function(evt) {
			var likeObj = {
				id: id,
				loc: userConnection.loc
			},
			numberOfVotes;

			evt.preventDefault();

			if ($(this).hasClass('pic-likes')) {
				likeObj.type = 'like';
				numberOfVotes = parseInt($('#' + id).find('.pic-likes .total').html());
				$('#' + id).find('.pic-likes .total').html(numberOfVotes += 1);
			} else if ($(this).hasClass('pic-dislikes')) {
				likeObj.type = 'dislike';
				numberOfVotes = parseInt($('#' + id).find('.pic-dislikes .total').html());
				$('#' + id).find('.pic-dislikes .total').html(numberOfVotes += 1);
			}
			
			wsc.send(JSON.stringify(likeObj));
		});

		$('#select-' + id).on('change', function(evt) {
			var flagObj = {
				type: 'flag',
				id: id,
				loc: userConnection.loc,
				name: ''
			};

			evt.preventDefault();

			if (this.value !== '') {
				flagObj.name = this.value;
				wsc.send(JSON.stringify(flagObj));
			}
		});
	},
	incomingLike = function(voteObj) {
		var id =  voteObj.id,
		likes = parseInt($('#' + id).find('.pic-likes .total').html());

		$('#' + id).find('.pic-likes .total').html(likes += 1);
	},
	incomingDislike = function(voteObj) {
		var id =  voteObj.id,
		dislikes = parseInt($('#' + id).find('.pic-dislikes .total').html());

		$('#' + id).find('.pic-dislikes .total').html(dislikes += 1);
	};

	filepicker.setKey('AgiFnrMKOQdmk5IbcPEpQz');

	if (window.File && window.FileReader && window.FileList && window.Blob) {
	    $('#file').on('change', function(evt) {
	    	fileSelect(evt);
	    });
	}

	/* Connection is established send location data */
	wsc.onopen = function(r) {
		console.log(r);
		wsc.send(JSON.stringify(userConnection));

		loadingTimer = setInterval(function() {
			if ($('#loading')) {
				if ($('#piclistings > ul').children('li').length > 0) {
					$('#loading').remove();
					$('#piclistings').removeClass('hidden');
				} else {
					$('#loading').html("<h1>No Images Nearby</h1>");
				}
			} else {
				clearInterval(loadingTimer);
			}
		}, 1500);
	}

	wsc.onmessage = function(r) {
		console.log(r);

		r.json = JSON.parse(r.data);

		if (r.json.type === 'piclisting') {
			addPic(r.json);
		} else if (r.json.type === 'liked') {
			incomingLike(r.json);
		} else if (r.json.type === 'disliked') {
			incomingDislike(r.json);
		}
	}

	wsc.onerror = function(r) {
		console.log(r)
	}

	wsc.onclose = function(r) {
		console.log(r)
	}



	$('#uploadBtn').on('click', function(evt) {
		var picObj = {
			type: 'pic',
			name: 'No Title',
			imgPath: '', 
			likes: 1,
			dislikes: 0,
			description: '',
			loc: {lng: 38.0297, lat: 84.4947}
		};

		evt.preventDefault();
	  
		filepicker.pick({
	    	mimetypes: ['image/*'],
	    	container: 'modal',
	    	maxSize: 4194304,
	    	services: [
	    		'WEBCAM',
	    		'COMPUTER',
		    	'BOX',
			    'DROPBOX',
			    'EVERNOTE',
			    'FACEBOOK',
			    'FLICKR',
			    'FTP',
			    'GOOGLE_DRIVE',
			    'SKYDRIVE',
			    'PICASA'
		   	],
		}, function(inkBlob){
			picObj.imgPath = inkBlob.url;
			picObj.description = inkBlob.filename;
	    	wsc.send(JSON.stringify(picObj));

			filepicker.read(inkBlob, {base64encode: true}, function(imgdata) {
	            addPic(picObj);
	        }, function(fperror) {

	        });
		}, function(FPError){

		});
	});
});